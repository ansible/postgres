# PostgreSQL

This Ansible role installs PostgreSQL from the main Ubuntu repos or from
the PostgreSQL repos (set `psql_install_repository: true` for the latter).
Installing from the PostgreSQL repos allows greater freedom in which version
to install.

This role has also been written to configure Let's Encrypt certbot certificates,
which is required because my mailserver playbook depends on this role.
The tasks in question only run if the Ansible target node belongs to the
`mailserver` group.


## postgresql cheat sheet

Connect to psql database from the terminal
```
taha@dahab:~
$ sudo -i -u postgres
postgres@dahab:~$ psql
psql (14.7 (Ubuntu 14.7-1.pgdg22.04+1))
Type "help" for help.

postgres=#
```
(or without interactive terminal and one go `sudo -u postgres psql`).

List all databases
```
postgres-# \l
                                  List of databases
    Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges
------------+----------+----------+-------------+-------------+-----------------------
 mailserver | mailuser | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 postgres   | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 template0  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
            |          |          |             |             | postgres=CTc/postgres
 template1  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
            |          |          |             |             | postgres=CTc/postgres
```

Enter a database
```
postgres=# \c mailserver
You are now connected to database "mailserver" as user "postgres".
```

Show all tables in the current schema
```
mailserver-# \dt
              List of relations
 Schema |      Name       | Type  |  Owner
--------+-----------------+-------+----------
 public | virtual_aliases | table | mailuser
 public | virtual_domains | table | mailuser
 public | virtual_users   | table | mailuser
```

Drop a database
```
sudo -u postgres dropdb <database_name>
```

+ https://phoenixnap.com/kb/how-to-connect-postgresql-database-command-line
+ https://stackoverflow.com/questions/769683/postgresql-show-tables-in-postgresql
+ https://stackoverflow.com/questions/7073773/how-to-drop-postgresql-database-through-command-line/7073852#7073852
