# How to dump and restore a database


## Dump using `pg_dump`


Dump a database into a custom-format archive suitable for input into `pg_restore`.
This format is compressed by default.
```
$ pg_dump -Fc mydb > db.dump
```

To dump a database called `mydb` into an SQL-script file:
```
$ pg_dump -Fp mydb > db.sql
```
(`-Fp` can be excluded as it is the default format).



## Restore using `pg_restore`

If database dump was created with `-Fc`, we can restore it with either

```
$ dropdb mydb
$ pg_restore -C -d postgres db.dump
```
(if the database `mydb` does not exist, notice the use of `-C -d postgres` to
allow the restore command permission to issue `CREATE DATABASE`) or with

```
$ createdb -T template0 newdb
$ pg_restore -d newdb db.dump
```



## Restore using `psql` tool

The database which you are trying to restore should exist (if not, create it first).

> If your database cluster has any local additions to the `template1` database,
> be careful to restore the output of `pg_dump` into a truly empty database;
> otherwise you are likely to get errors due to duplicate definitions of the added
> objects. To make an empty database without any local additions, copy from
> `template0` not `template1`, for example:

```
CREATE DATABASE foo WITH TEMPLATE template0;
```

Restore:
```
psql postgres < myBackupFile
```



## Refs

+ https://www.postgresql.org/docs/14/app-pgdump.html
+ https://www.postgresql.org/docs/14/app-pgrestore.html
+ https://www.educba.com/postgres-dump-database/
