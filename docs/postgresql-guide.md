# PostgreSQL commands

with example output.


## Login as the postgres user

```
$ sudo -u postgres psql
psql (14.1 (Ubuntu 14.1-2.pgdg20.04+1), server 12.9 (Ubuntu 12.9-2.pgdg20.04+1))
Type "help" for help.

postgres=#
```


## List all databases

```
postgres=# \list
                                  List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges
-----------+----------+----------+-------------+-------------+-----------------------
 postgres  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 template0 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
(3 rows)
```

You can also use the shortcut `\l`.


### Switch to a database

```
postgres=# \c mydb
```


### List all tables

```
postgres=# \dt
```

or for more verbose output, `\dt+`.


### Show description of specific table

```
postgres=# \d mytable
```



## Create database

```
postgres=# create database mydb;
CREATE DATABASE
```


## Create a user with password

```
postgres=# create user solarchemist with password 'somesecretpassword';
CREATE ROLE
postgres=# grant all privileges on database mydb to solarchemist;
GRANT
```


## List all users

```
postgres=# \du
                                   List of roles
 Role name    |                         Attributes                         | Member of
--------------+------------------------------------------------------------+-----------
 postgres     | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
 solarchemist | Create role, Create DB                                    +| {}
              | Password valid until infinity                              |
```





## Refs

+ https://dev.to/addupe/differences-between-postgresql-and-mysql-3jck
